import torch # type: ignore[attr-defined]

import torch.nn as nn # type: ignore[attr-defined]
import torch_geometric.nn as gnn # type: ignore[attr-defined]

import torch.nn.functional as F # type: ignore[attr-defined]
import torch_geometric.utils as U # type: ignore[attr-defined]
from torch_geometric.loader import DataLoader # type: ignore[attr-defined]

import numpy as np # type: ignore[attr-defined]

class Model(nn.Module):
    def __init__(self, gatwidth=128, featlen=14):
        super().__init__()
        self.gatwidth = gatwidth
        self.featlen = 14
        
        self.preproc = nn.Sequential(    
            nn.Linear(featlen, gatwidth),
            nn.ReLU(),
            nn.Linear(gatwidth, gatwidth),
            nn.ReLU(),
        )
        
        self.gat1 = gnn.GATv2Conv(in_channels=gatwidth, out_channels=gatwidth)
        self.gat2 = gnn.GATv2Conv(in_channels=gatwidth, out_channels=gatwidth)
        self.gat3 = gnn.GATv2Conv(in_channels=gatwidth, out_channels=gatwidth)
        
        self.vhead = nn.Sequential(
            nn.Linear(gatwidth * 4, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 1),
            nn.Sigmoid(),
        )
        
        self.phead = nn.Sequential(
            nn.Linear(gatwidth * 4, 256),
            nn.ReLU(),
            nn.Linear(256, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
        )

    def forward(self, x, edge_index, batch):
        x0 = self.preproc(x)
        
        x1 = self.gat1(x0, edge_index)
        x2 = self.gat2(x1, edge_index)
        x3 = self.gat3(x2, edge_index)

        pooled = gnn.global_add_pool(torch.cat([x0, x1, x2, x3], dim=1), batch)
        v = self.vhead(pooled)
        v = torch.squeeze(v, dim=1)
        
        p = self.phead(torch.cat([x0, x1, x2, x3], dim=1))
        p = U.softmax(p, batch)
        p = torch.squeeze(p)
        
        return v, p

def vloss(truth: torch.Tensor, v: torch.Tensor) -> torch.Tensor:
    """
    Computes value loss.
    
    Tensor shapes:
    * truth: [B]
    * v: [B]
    * output: []

    B is the batch size.
    """
    loss = F.mse_loss(v, truth, reduction='mean')
    return torch.squeeze(loss)

def ploss(truth: torch.Tensor, p: torch.Tensor, batch_size: int) -> torch.Tensor:
    """
    Computes policy loss.
    
    Tensor shapes:
    * truth: [N]
    * p: [N]
    * output: []

    N is the total number of nodes in the batch.
    """
    loss_sum = -torch.sum(truth * torch.log(1e-7 + p))
    return torch.squeeze(loss_sum / batch_size)

def train(model, opt, data_list):
    loader = DataLoader(data_list, batch_size=32, shuffle=True)
    losses = []
    for batch in loader:
        batch = batch.to('cuda')

        # v: [B]
        # p: [V]
        v, p = model.forward(batch.x, batch.edge_index, batch.batch)
        vl = vloss(truth=batch.value, v=v)
        pl = ploss(truth=batch.policy, p=p, batch_size=batch.num_graphs)
        loss = vl + pl

        losses.append([loss.item(), vl.item(), pl.item()])

        # backprop
        opt.zero_grad()
        loss.backward()
        opt.step()

    return np.array(losses).mean(axis=0)
