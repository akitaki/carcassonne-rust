#! /usr/bin/env python3
import csv
from typing import List, TypeVar, TypedDict, cast

class Row(TypedDict):
    id: str
    name: str
    edge_features: str
    feature_ids: str
    pennant: str
    cloister: str
    feature_pennants: str
    feature_kinds: str
    field_adj_cities: str
    num_tiles: str
    allowed_rotations: str

T = TypeVar('T')
def rotate(l: List[T], n: int) -> List[T]:
    return l[n:] + l[:n]

def pad(li: List[T], elem: T, to_len: int) -> List[T]:
    return li + [elem] * (to_len - len(li))

# Read csv into the list
data: List[Row] = []
with open('./tiles.csv') as file:
    reader = csv.DictReader(file)
    for r in reader:
        row: Row = cast(Row, r)
        data.append(row)

print('''
use super::{Feature, FeatureId, Rotation};
use serde::{Serialize, Deserialize};
use strum_macros::EnumIter;
use strum::IntoEnumIterator;
use num_enum::IntoPrimitive;
use num_traits::FromPrimitive;
#[allow(non_upper_case_globals)]
const c: Feature = Feature::City;
#[allow(non_upper_case_globals)]
const f: Feature = Feature::Field;
#[allow(non_upper_case_globals)]
const r: Feature = Feature::Road;
''')

# enum TileKind
print('#[derive(Debug, Copy, Clone, Eq, PartialEq, EnumIter, IntoPrimitive, Serialize, Deserialize, Hash)]')
print('#[repr(u8)]')
print('pub enum TileKind {')
for row in data:
    print(f'    {row["name"]} = {row["id"]},')
print('}')

# impl TileKind
print('''impl TileKind {
    pub(crate) fn num_features(&self) -> usize {
        MAX_FEATURE_ID[*self as u8 as usize] + 1
    }
    pub(crate) fn num_tiles(&self) -> usize {
        NUM_TILES[*self as u8 as usize]
    }
    pub(crate) fn feature_ids(&self) -> impl Iterator<Item = FeatureId> {
        FeatureId::iter().take(self.num_features())
    }
    pub(crate) fn allowed_rotations(&self) -> impl Iterator<Item = Rotation> {
        ALLOWED_ROTATIONS[*self as u8 as usize].iter().map(|&num| Rotation::from_u8(num).unwrap())
    }
}''')

# Image data as bytes slice
# [kind] -> bytes
print('pub(crate) static TILE_IMAGE_BYTES: [&[u8]; 24] = [')
for row in data:
    print(f'    include_bytes!("../../resources/tiles/{row["name"]}.png"),')
print('];')

# [kind, rotation, segment] -> feature kind
print('pub(crate) static EDGE_FEATURES: [[[Feature; 12]; 4]; 24] = [')
for row in data:
    print('    [')
    li = row['edge_features'].split(',')
    print(f'        // ({row["id"]}) {row["name"]}')
    for i in [0, 3, 6, 9]:
        print('        [' + ','.join(rotate(li, i)) + '],')
    print('    ],')
print('];')

# [kind, rotation, segment] -> feature id
print('pub(crate) static FEATURE_IDS: [[[u8; 12]; 4]; 24] = [')
for row in data:
    print('    [')
    li = row['feature_ids'].split(',')
    print(f'        // ({row["id"]}) {row["name"]}')
    for i in [0, 3, 6, 9]:
        print('        [' + ','.join(rotate(li, i)) + '],')
    print('    ],')
print('];')

# [kind] -> int
print('pub(crate) static MAX_FEATURE_ID: [usize; 24] = [')
for row in data:
    ili = list(map(lambda s: int(s), row['feature_ids'].split(',')))
    print('    ' + str(max(ili)) + f', // ({row["id"]}) {row["name"]}')
print('];')

# [kind] -> int
print('pub(crate) static ALLOWED_ROTATIONS: [&[u8]; 24] = [')
for row in data:
    ili = list(map(lambda s: int(s), row['allowed_rotations'].split(',')))
    print('    &' + str(ili) + ', ')
print('];')

# [kind, feature id] -> [feature id; 2]
print('pub(crate) static FIELD_FEATURE_ADJ_CITIES: [[[u8; 2]; 8]; 24] = [')
for row in data:
    print('    [')
    s = ','.join(pad(
        list(map(
            lambda feat: '[' + ','.join(pad(
                ['0' if s == '' else s for s in feat.split(',')],
                '0',
                2
            )) + ']',
            row['field_adj_cities'].split(';')
        )),
        '[0,0]',
        8
    ))
    print(f'        {s},')
    print('    ],')
print('];')

# [kind, feature id] -> int
print('pub(crate) static FIELD_FEATURE_NUM_ADJ_CITIES: [[usize; 8]; 24] = [')
for row in data:
    print('    [')
    empties_removed = [[] if y == [''] else y for y in [
        x.split(',')
        for x in row['field_adj_cities'].split(';')
    ]]
    counted = [
        str(len(x))
        for x in empties_removed
    ]
    padded = pad(counted, '0', 8)
    print('        ' + ','.join([','.join(x) for x in padded]))
    print(f'    ], // ({row["id"]}) {row["name"]}')
print('];')

# [kind, feature id] -> bool
print('pub(crate) static HAS_PENNANT: [[bool; 8]; 24] = [')
for row in data:
    li = pad(row['feature_pennants'].split(','), '0', 8)
    li = ['true' if num == '1' else 'false' for num in li]
    print('    [' + ','.join(li) + f'], // ({row["id"]}) {row["name"]}')
print('];')

print('pub(crate) static FEATURE_KINDS: [[Feature; 8]; 24] = [')
for row in data:
    li = pad(row['feature_kinds'].split(','), 'f', 8)
    print('    [' + ','.join(li) + f'], // ({row["id"]}) {row["name"]}')
print('];')

print('pub(crate) static HAS_CLOISTER: [bool; 24] = [')
for row in data:
    li = row['pennant'].split(',')
    print(f'    {"true" if row["cloister"] == "1" else "false"}, // ({row["id"]}) {row["name"]}')
print('];')

print('pub(crate) static NUM_TILES: [usize; 24] = [')
for row in data:
    print(f'    {row["num_tiles"]}, // ({row["id"]}) {row["name"]}')
print('];')
