use std::fmt::Debug;
use std::time::Duration;

use anyhow::Result;

use tokio::{
    sync::mpsc::{channel, Receiver, Sender},
    time::timeout,
};
use tracing::error;

#[derive(Debug)]
pub struct Host<U: Debug, V: Debug> {
    res_sends: Vec<Sender<V>>,
    req_recv: Receiver<Request<U>>,
    remain: usize,
    max_batch_size: usize,
    timeout: Duration,
}

/// Convenience type to do response concurrently
#[derive(Debug)]
pub struct Responder<V: Debug> {
    res_sends: Vec<Sender<V>>,
}

#[derive(Debug)]
pub struct Client<U: Debug, V: Debug> {
    req_send: Sender<Request<U>>,
    res_recv: Receiver<V>,
    id: ClientId,
}

#[derive(Debug)]
pub struct ClientId(usize);

#[derive(Debug)]
struct Request<U: Debug> {
    id: ClientId,
    data: U,
}

impl<U: Debug, V: Debug> Client<U, V> {
    pub async fn request(&self, request: U) {
        // manually clone the id
        let id = ClientId(self.id.0);
        let res = self.req_send.send(Request { id, data: request }).await;
        if let Err(ref e) = res {
            error!("Client failed to send request: {:#?}", e);
        }
    }

    pub async fn get_response(&mut self) -> V {
        let res = self.res_recv.recv().await;
        if res.is_none() {
            error!("Client failed to get response");
        }
        res.unwrap()
    }
}

impl<U: Debug, V: Debug> Host<U, V> {
    /// num_clients: Number of client handles to create
    /// max_batch_size: Maximum number of requests to collect before returning the batch to host
    pub fn create(
        num_clients: usize,
        max_batch_size: usize,
        timeout: Duration,
    ) -> (Host<U, V>, Vec<Client<U, V>>) {
        let mut clients = vec![];
        let mut res_sends = vec![];
        let (req_send, req_recv) = channel(1);
        for id in 0..num_clients {
            let (res_send, res_recv) = channel(1);
            clients.push(Client {
                req_send: req_send.clone(),
                res_recv,
                id: ClientId(id),
            });
            res_sends.push(res_send);
        }
        let host = Self {
            res_sends,
            req_recv,
            max_batch_size,
            timeout,
            remain: num_clients,
        };
        (host, clients)
    }

    /// Collect a batch or requests.  This function blocks until
    ///
    /// 1. All remaining clients have sent a request.
    /// 2. Number of collected requests exeeded the theshold (see [`Host::create`]).
    /// 3. All clients have been dropped (which is just a special case of 1).
    /// 4. Timeout on further receives when some requests are already received.
    ///
    /// To ensure that this doesn't block indefinitely, make sure to drop the clients
    /// once they don't make requests anymore.
    pub async fn get_requests(&mut self) -> Option<(Vec<ClientId>, Vec<U>)> {
        let mut ids = vec![];
        let mut requests = vec![];

        while requests.len() < self.remain && requests.len() < self.max_batch_size {
            // None means timeout
            let msg: Option<Request<U>> = if requests.is_empty() {
                self.req_recv.recv().await
            } else {
                timeout(self.timeout, self.req_recv.recv())
                    .await
                    .ok()
                    .flatten()
            };
            match msg {
                Some(Request { id, data: request }) => {
                    ids.push(id);
                    requests.push(request);
                }
                None => {
                    // timeout
                    break;
                }
            }
        }
        if ids.is_empty() {
            None
        } else {
            Some((ids, requests))
        }
    }

    pub async fn respond(&self, ids: Vec<ClientId>, responses: Vec<V>) -> Result<()> {
        for (id, response) in ids.into_iter().zip(responses.into_iter()) {
            self.res_sends[id.0].send(response).await.unwrap();
        }
        Ok(())
    }

    pub fn responder(&self) -> Responder<V> {
        Responder {
            res_sends: self.res_sends.clone(),
        }
    }
}

impl<V: Debug> Responder<V> {
    pub async fn respond(&self, ids: Vec<ClientId>, responses: Vec<V>) -> Result<()> {
        for (id, response) in ids.into_iter().zip(responses.into_iter()) {
            self.res_sends[id.0].send(response).await.unwrap();
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rand::{thread_rng, Rng};
    use tokio::{task, time::sleep};

    #[tokio::test]
    async fn mailbox_integration() {
        const NUM_CLIENTS: usize = 16;
        const BATCH_SIZE: usize = 4;
        let (mut host, clients) =
            Host::<usize, usize>::create(NUM_CLIENTS, BATCH_SIZE, Duration::from_secs(100));

        let mut ts = vec![];
        for (tid, mut client) in clients.into_iter().enumerate() {
            let t = task::spawn(async move {
                let millis = thread_rng().gen_range(200..=500);
                sleep(Duration::from_millis(millis)).await;

                println!("Task {tid} sending request", tid = tid);
                client.request(tid).await;
                let resp = client.get_response().await;
                println!("Task {tid} got response {resp}", tid = tid, resp = resp);
                resp
            });
            ts.push(t);
        }

        let mut inference_count = 0;
        while let Some((ids, requests)) = host.get_requests().await {
            let sum = requests.iter().sum();
            let responses = vec![sum; ids.len()];
            host.respond(ids, responses).await.unwrap();
            inference_count += 1;
        }
        assert_eq!(inference_count, NUM_CLIENTS / BATCH_SIZE);

        let mut sum = 0;
        for t in ts.into_iter() {
            sum += t.await.unwrap();
        }
        assert_eq!(sum, (0..NUM_CLIENTS).sum::<usize>() * BATCH_SIZE);
    }
}
