#!/bin/sh

TMPDIR=/var/tmp podman run \
    --rm -it \
    -v $PWD:/data/ \
    --runtime=nvidia -e NVIDIA_VISIBLE_DEVICES=all \
    -p 8888:8888 \
    -p 12000:12000 \
    --name gnn \
    docker.io/akitaki/gnn:latest
