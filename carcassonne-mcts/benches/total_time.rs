use criterion::{black_box, criterion_group, criterion_main, Criterion};

use rand::prelude::*;

use ccs::Board;
use ccs_mcts::Mcts;

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut boards = vec![];
    while boards.len() < 10000 {
        let mut board = Board::new();

        while board.is_ended() == false {
            let mut draw_times = 0;
            loop {
                board.draw();
                let legal_actions = board.legal_actions();
                if legal_actions.is_empty() == false {
                    break;
                }

                draw_times += 1;
                if draw_times >= 1000 {
                    panic!("All remaining tiles have no legal positions");
                }
            }
            let action = *board.legal_actions().choose(&mut thread_rng()).unwrap();

            boards.push(board.clone());
            board.do_action(action);
        }
    }
    c.bench_function("Mcts 500 iters", |b| {
        b.iter(|| {
            let board = boards.choose(&mut thread_rng()).unwrap().clone();
            black_box(Mcts::new().mcts_loop(board, 500));
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
