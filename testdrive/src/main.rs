use std::{fmt::Display, sync::Arc, time::Duration};

use anyhow::Result;
use async_trait::async_trait;
use futures::{
    future::{try_join_all, BoxFuture},
    FutureExt,
};
use once_cell::sync::OnceCell;
use rand::prelude::*;
use structopt::{clap::arg_enum, StructOpt};
use tokio::{sync::RwLock, task};
use tracing::info;

use ccs::{Action, Board, Player};
use ccs_gnn::{mcts::Mcts as GnnMcts, EvalClient, EvalHost};
use ccs_mcts::Mcts;

#[cfg(not(target_env = "msvc"))]
use jemallocator::Jemalloc;

#[cfg(not(target_env = "msvc"))]
#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

type Shared<T> = Arc<RwLock<T>>;

static ARGS: OnceCell<Args> = OnceCell::new();

#[derive(Debug, StructOpt)]
struct Args {
    #[structopt(
        long = "address",
        short = "a",
        default_value = "http://127.0.0.1:12000"
    )]
    address: String,
    #[structopt(
        long = "tasks",
        short = "t",
        default_value = "8",
        help = "Number of concurrent pitting tasks"
    )]
    pit_tasks: usize,
    #[structopt(
        long = "batchsize",
        short = "b",
        default_value = "4",
        help = "Inference batch size"
    )]
    batch_size: usize,
    #[structopt(long = "mcts", short = "m", default_value = "500")]
    mcts_loop_times: usize,
    #[structopt(long = "gnn", short = "g", default_value = "500")]
    gnn_loop_times: usize,

    #[structopt(short = "n", help = "Number of combats")]
    num_combats: usize,

    #[structopt(long = "sa")]
    solver_a: SolverKind,
    #[structopt(long = "sb")]
    solver_b: SolverKind,

    #[structopt(long = "size", default_value = "BoardSize::Small")]
    board_size: BoardSize,
}

arg_enum! {
    #[derive(Debug)]
    enum SolverKind {
        Rand,
        Mcts,
        Gnn,
    }
}

arg_enum! {
    #[derive(Debug, PartialEq, Eq)]
    enum BoardSize {
        Small,Normal
    }
}

impl BoardSize {
    fn create_board(&self) -> Board {
        match self {
            BoardSize::Small => Board::new_small(),
            BoardSize::Normal => Board::new(),
        }
    }
}

struct MctsSolver {
    iters: usize,
}

#[async_trait]
impl Solver for MctsSolver {
    async fn solve(&mut self, board: Board) -> Action {
        let mut mcts = Mcts::new();
        let action = mcts.mcts_loop(board.clone(), self.iters);
        mcts.cleanup_arena();
        action
    }
}

impl Display for MctsSolver {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Mcts({})", self.iters)
    }
}

struct RandSolver {}

#[async_trait]
impl Solver for RandSolver {
    async fn solve(&mut self, board: Board) -> Action {
        *board.legal_actions().choose(&mut thread_rng()).unwrap()
    }
}

impl Display for RandSolver {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Rand")
    }
}

struct GnnSolver {
    mcts: GnnMcts,
    iters: usize,
}

#[async_trait]
impl Solver for GnnSolver {
    async fn solve(&mut self, board: Board) -> Action {
        let action = self.mcts.mcts_loop(board.clone(), self.iters).await;
        self.mcts.cleanup_arena();
        action
    }
}

impl Display for GnnSolver {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Gnn({})", self.iters)
    }
}

impl GnnSolver {
    fn new(client: EvalClient, iters: usize) -> Self {
        GnnSolver {
            mcts: GnnMcts::new(client),
            iters,
        }
    }
}

#[async_trait]
trait Solver: Send + Display {
    async fn solve(&mut self, board: Board) -> Action;
}

#[tokio::main(flavor = "multi_thread", worker_threads = 64)]
async fn main() -> Result<()> {
    let _guard = init_tracing()?;

    ARGS.set(Args::from_args()).unwrap();

    let mut tasks = vec![];

    let args = ARGS.get().unwrap();
    let (mut host, clients) =
        EvalHost::new(args.pit_tasks, args.batch_size, Duration::from_millis(10)).await?;

    let next_num = Arc::new(RwLock::new(0));
    let collected = Arc::new(RwLock::new(vec![]));
    for (tid, client) in clients.into_iter().enumerate() {
        let next_num = next_num.clone();
        let collected = collected.clone();
        tasks.push(task::spawn(async move {
            pit_loop(tid, client, next_num, collected).await
        }));
    }

    host.start_batch_loop().await?;
    try_join_all(tasks).await?;

    let mut scores = [0, 0, 0];
    for &index in collected.read().await.iter() {
        scores[index] += 1;
    }

    info!("Scores: {:?}", scores);

    Ok(())
}

async fn pit_loop(
    _tid: usize,
    client: EvalClient,
    next_num: Shared<usize>,
    collected: Shared<Vec<usize>>,
) {
    let args = ARGS.get().unwrap();
    let mut sa: Box<dyn Solver + Send> = match args.solver_a {
        SolverKind::Rand => Box::new(RandSolver {}),
        SolverKind::Mcts => Box::new(MctsSolver {
            iters: args.mcts_loop_times,
        }),
        SolverKind::Gnn => Box::new(GnnSolver::new(client, args.gnn_loop_times)),
    };
    let mut sb: Box<dyn Solver> = match args.solver_b {
        SolverKind::Rand => Box::new(RandSolver {}),
        SolverKind::Mcts => Box::new(MctsSolver {
            iters: args.mcts_loop_times,
        }),
        _ => panic!("Only solver A can be using gnn"),
    };

    loop {
        let i: usize = {
            let mut next_num = next_num.write().await;
            let i = *next_num;
            *next_num += 1;
            i
        };

        if i >= ARGS.get().unwrap().num_combats {
            return;
        }

        let (winner, scores) = if i % 2 == 0 {
            pit(&mut *sa, &mut *sb).await
        } else {
            pit(&mut *sb, &mut *sa).await
        };
        let idx = match winner {
            Some(Player::Red) => {
                if i % 2 == 0 {
                    info!("Game {}: {} (red) {:?}", i, sa.to_string(), scores);
                    0
                } else {
                    info!("Game {}: {} (red) {:?}", i, sb.to_string(), scores);
                    1
                }
            }
            Some(Player::Blue) => {
                if i % 2 == 0 {
                    info!("Game {}: {} (blue) {:?}", i, sb.to_string(), scores);
                    1
                } else {
                    info!("Game {}: {} (blue) {:?}", i, sa.to_string(), scores);
                    0
                }
            }
            None => {
                info!("Game {}: tie {:?}", i, scores);
                2
            }
        };

        {
            let mut collected = collected.write().await;
            collected.push(idx);
        }
    }
}

fn pit<'sa>(
    sa: &'sa mut dyn Solver,
    sb: &'sa mut dyn Solver,
) -> BoxFuture<'sa, (Option<Player>, [u8; 2])> {
    async move {
        let mut board = ARGS.get().unwrap().board_size.create_board();
        let mut i = 0;

        while board.is_ended() == false {
            i += 1;

            let mut draw_times = 0;
            loop {
                board.draw();
                let legal_actions = board.legal_actions();
                if legal_actions.is_empty() == false {
                    break;
                }

                draw_times += 1;
                if draw_times >= 1000 {
                    return pit(sa, sb).await;
                }
            }
            let action = if i % 2 == 1 {
                sa.solve(board.clone()).await
            } else {
                sb.solve(board.clone()).await
            };

            board.do_action(action);
        }

        (board.winner(), board.scores())
    }
    .boxed()
}

fn init_tracing() -> Result<tracing_appender::non_blocking::WorkerGuard> {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{fmt, EnvFilter};

    let file_appender = tracing_appender::rolling::hourly("logs", "carcassonne.log");
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);

    let collector = tracing_subscriber::registry()
        .with(EnvFilter::from_default_env())
        .with(fmt::Layer::new().with_writer(std::io::stdout))
        .with(fmt::Layer::new().with_writer(non_blocking));
    tracing::subscriber::set_global_default(collector)?;

    Ok(_guard)
}
