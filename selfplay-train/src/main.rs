use std::collections::VecDeque;
use std::fs::File;
use std::path::Path;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{bail, Result};
use async_trait::async_trait;
use ccs::{Action, Board};
use ccs_gnn::convert::{graph_from_board, GraphConvertOutput};
use ccs_gnn::mcts::Mcts as GnnMcts;
use ccs_gnn::{EvalClient, EvalHost, Sample, TrainHost};
use ccs_mcts::Mcts as PureMcts;
use chrono::Local;
use futures::future::try_join_all;
use hashbrown::HashMap;
use ndarray::prelude::*;
use once_cell::sync::OnceCell;
use rand::prelude::*;
use structopt::StructOpt;
use tokio::fs::create_dir_all;
use tokio::sync::Mutex;
use tokio::task;
use tracing::{error, info, warn};

static ARGS: OnceCell<Args> = OnceCell::new();

#[derive(Debug, StructOpt)]
struct Args {
    #[structopt(
        long = "address",
        short = "a",
        default_value = "http://127.0.0.1:12000"
    )]
    address: String,
    #[structopt(long = "tasks", short = "t", default_value = "8")]
    selfplay_tasks: usize,
    #[structopt(long = "batchsize", short = "b", default_value = "4")]
    batch_size: usize,
    #[structopt(long = "mcts", short = "m", default_value = "500")]
    mcts_loop_times: usize,

    #[structopt(long = "gnn", short = "g")]
    use_gnn: bool,

    #[structopt(
        long = "every",
        short = "e",
        help = "Number of games between trainings"
    )]
    every: usize,
}

#[tokio::main]
async fn main() -> Result<()> {
    let _guard = init_tracing()?;
    ARGS.set(Args::from_args()).unwrap();
    let args = ARGS.get().unwrap();

    let mut samples = SampleQueue {
        samples: Mutex::new(VecDeque::new()),
        max_len: 10000,
    };
    if Path::new("./samples.bin").is_file() {
        samples.load("./samples.bin").await?;
    }
    let samples = Arc::new(samples);

    // load samples

    loop {
        let counter = Arc::new(Counter {
            count: Mutex::new(0),
        });

        // setup and selfplay
        let mut tasks = vec![];
        let (mut host, clients) = EvalHost::new(
            args.selfplay_tasks,
            args.batch_size,
            Duration::from_micros(500),
        )
        .await?;

        for (tid, client) in clients.into_iter().enumerate() {
            let (samples, counter) = (samples.clone(), counter.clone());
            tasks.push(task::spawn(async move {
                selfplay_loop(tid, client, samples, counter).await
            }));
        }

        host.start_batch_loop().await?;
        try_join_all(tasks).await?;

        drop(host);
        // training

        let host = TrainHost::load("./saves/model.pt")?;

        info!("Saving samples");
        samples.save("./samples.bin").await?;

        info!("Training on {} samples", samples.len().await);
        let mut samples = samples.samples.lock().await;
        host.train(samples.make_contiguous(), 4)?;

        create_dir_all("./saves").await?;
        host.save("./saves/model.pt")?;
        host.save(&format!(
            "./saves/model.{}.pt",
            Local::now().format("%Y%m%d-%H%M%S")
        ))?;
    }
}

async fn selfplay_loop(
    tid: usize,
    client: EvalClient,
    samples: Arc<SampleQueue>,
    counter: Arc<Counter>,
) {
    let mut solver: Box<dyn MctsSolver> = if ARGS.get().unwrap().use_gnn {
        Box::new(GnnMctsSolver {
            mcts: GnnMcts::new(client).noised(),
        })
    } else {
        Box::new(PureMctsSolver {
            mcts: PureMcts::new(),
        })
    };

    while counter.tick().await {
        if let Err(e) = selfplay(&mut *solver, samples.clone()).await {
            error!(
                "Selfplay task {tid} encountered an error: {e:#?}",
                tid = tid,
                e = e
            );
        } else {
            info!("Selfplay task {tid} submitted a game", tid = tid);
        }
    }

    info!("Selfplay task {tid} exited", tid = tid);
}

async fn selfplay(mcts: &mut dyn MctsSolver, samples: Arc<SampleQueue>) -> Result<()> {
    const DRAW_RETRY_THRESHOLD: usize = 10;
    let mcts_loop_times = ARGS.get().unwrap().mcts_loop_times;
    let mut board = Board::new_small();

    // vector of (board, prob hashmap, turn)
    let mut history = vec![];
    while !board.is_ended() {
        // try to draw card
        let mut draw_times = 0;
        loop {
            board.draw();
            let legal_actions = board.legal_actions();
            if legal_actions.is_empty() == false {
                break;
            }

            draw_times += 1;
            if draw_times >= DRAW_RETRY_THRESHOLD {
                warn!(
                    "Failed to find draw with legal positions within {} trials",
                    DRAW_RETRY_THRESHOLD
                );
                bail!("No legal actions during selfplay");
            }
        }

        // query prob
        mcts.mcts_loop(board.clone(), mcts_loop_times).await;
        let prob = mcts.action_prob();
        mcts.cleanup_arena();

        let mut candidates = vec![];

        // create vector to choose from
        for (&action, &score) in prob.iter() {
            candidates.push((action, score));
        }

        let (action, _score) = candidates
            .choose_weighted(&mut thread_rng(), |(_, score)| *score)
            .unwrap();

        // training uses the original search output with temp = 1.0
        history.push((board.clone(), prob, board.turn()));

        board.do_action(*action);
    }

    info!("A game ended with winner {:?}", board.winner());

    // record
    for (board, prob, turn) in history.into_iter() {
        let score: f32 = match board.winner() {
            Some(winner) if winner == turn => 1.0,
            Some(_) => 0.0,
            None => 0.5,
        };
        let GraphConvertOutput {
            graph,
            candidate_node_indexes,
            nodes_len,
        } = graph_from_board(board);

        let value = ArrayD::from_shape_vec(IxDyn(&[1]), vec![score]).unwrap();
        let policy = policy_from_prob(prob, candidate_node_indexes, nodes_len);
        samples
            .store(Sample {
                graph,
                policy,
                value,
            })
            .await;
    }

    Ok(())
}

#[async_trait]
trait MctsSolver: Send {
    async fn mcts_loop(&mut self, from_state: Board, iters: usize) -> Action;
    fn action_prob(&self) -> HashMap<Action, f32>;
    fn cleanup_arena(&mut self);
}

struct PureMctsSolver {
    mcts: PureMcts,
}

struct GnnMctsSolver {
    mcts: GnnMcts,
}

#[async_trait]
impl MctsSolver for PureMctsSolver {
    async fn mcts_loop(&mut self, from_state: Board, iters: usize) -> Action {
        self.mcts.mcts_loop(from_state, iters)
    }

    fn action_prob(&self) -> HashMap<Action, f32> {
        self.mcts.action_prob()
    }

    fn cleanup_arena(&mut self) {
        self.mcts.cleanup_arena()
    }
}

#[async_trait]
impl MctsSolver for GnnMctsSolver {
    async fn mcts_loop(&mut self, from_state: Board, iters: usize) -> Action {
        self.mcts.mcts_loop(from_state, iters).await
    }

    fn action_prob(&self) -> HashMap<Action, f32> {
        self.mcts.action_prob()
    }

    fn cleanup_arena(&mut self) {
        self.mcts.cleanup_arena()
    }
}

fn policy_from_prob(
    prob: HashMap<Action, f32>,
    candidate_node_indexes: HashMap<Action, usize>,
    nodes_len: usize,
) -> ArrayD<f32> {
    let mut policy = ArrayD::from_shape_simple_fn(IxDyn(&[nodes_len]), || 0.0);
    for (action, index) in candidate_node_indexes {
        policy[index] = prob[&action];
    }
    policy
}

fn init_tracing() -> Result<tracing_appender::non_blocking::WorkerGuard> {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{fmt, EnvFilter};

    let file_appender = tracing_appender::rolling::hourly("logs", "alphazero.log");
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);

    let collector = tracing_subscriber::registry()
        .with(EnvFilter::from_default_env())
        .with(fmt::Layer::new().with_writer(std::io::stdout))
        .with(fmt::Layer::new().with_writer(non_blocking));
    tracing::subscriber::set_global_default(collector)?;

    Ok(_guard)
}

struct SampleQueue {
    samples: Mutex<VecDeque<Sample>>,
    max_len: usize,
}

impl SampleQueue {
    async fn store(&self, sample: Sample) {
        let mut samples = self.samples.lock().await;
        samples.push_back(sample);
        while samples.len() > self.max_len {
            samples.pop_front();
        }
    }

    async fn save(&self, path: impl AsRef<str>) -> Result<()> {
        let samples = self.samples.lock().await;

        let file = File::create(path.as_ref())?;
        bincode::serialize_into(file, &*samples)?;

        Ok(())
    }

    async fn load(&mut self, path: impl AsRef<str>) -> Result<()> {
        let file = File::open(path.as_ref())?;
        let samples = bincode::deserialize_from(file)?;
        self.samples = Mutex::new(samples);

        Ok(())
    }

    async fn len(&self) -> usize {
        self.samples.lock().await.len()
    }
}

struct Counter {
    count: Mutex<usize>,
}

impl Counter {
    async fn tick(&self) -> bool {
        let mut count = self.count.lock().await;
        *count += 1;
        if *count > ARGS.get().unwrap().every {
            false
        } else {
            true
        }
    }
}
